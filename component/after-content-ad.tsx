import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'

const AfterContentAds: NextPage = () => {
  return (
    <>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3619133031508264"
     crossOrigin="anonymous"></script>
    <ins className="adsbygoogle"
        style={{display: 'block'}}
        data-ad-client="ca-pub-3619133031508264"
        data-ad-slot="4988474894"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
    </>
  )
}

export default AfterContentAds
